import { MappingProperty, PropertyName } from '@elastic/elasticsearch/lib/api/types'
import { ActorImage } from '../../../../PeerTube/shared/models'
import { buildUrl } from '../../../helpers/utils'

function formatActorImageForAPI (image?: ActorImage) {
  if (!image) return null

  return {
    url: image.url,
    path: image.path,
    width: image.width,
    createdAt: image.createdAt,
    updatedAt: image.updatedAt
  }
}

function formatActorImagesForAPI (images?: ActorImage[], image?: ActorImage) {
  // Does not exist in PeerTube < 4.2
  if (!images) {
    if (!image) return []

    return [ image ]
  }

  return images.map(a => formatActorImageForAPI(a))
}

// ---------------------------------------------------------------------------

function formatActorImageForDB (image: ActorImage, host: string) {
  if (!image) return null

  return {
    url: buildUrl(host, image.path),
    path: image.path,
    width: image.width,
    createdAt: image.createdAt,
    updatedAt: image.updatedAt
  }
}

function formatActorImagesForDB (images: ActorImage[], host: string) {
  if (!images) return null

  return images.map(image => formatActorImageForDB(image, host))
}

// ---------------------------------------------------------------------------

function buildActorImageMapping () {
  return {
    path: {
      type: 'keyword'
    },
    width: {
      type: 'long'
    },
    createdAt: {
      type: 'date',
      format: 'date_optional_time'
    },
    updatedAt: {
      type: 'date',
      format: 'date_optional_time'
    }
  } as Record<PropertyName, MappingProperty>
}

export {
  formatActorImageForAPI,
  formatActorImagesForAPI,

  formatActorImageForDB,
  formatActorImagesForDB,

  buildActorImageMapping
}
