import Pino from 'pino'
import { CONFIG } from '../initializers/constants'

const pino = Pino({
  transport: {
    target: 'pino-pretty'
  },
  level: CONFIG.LOG.LEVEL
})

// ---------------------------------------------------------------------------

export {
  pino as logger
}
