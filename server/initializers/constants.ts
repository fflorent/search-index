import config from 'config'
import { isTestInstance } from '../helpers/core-utils'

const API_VERSION = 'v1'

const CONFIG = {
  LISTEN: {
    PORT: config.get<number>('listen.port')
  },
  WEBSERVER: {
    SCHEME: config.get<boolean>('webserver.https') === true ? 'https' : 'http',
    HOSTNAME: config.get<string>('webserver.hostname'),
    PORT: config.get<number>('webserver.port')
  },
  ELASTIC_SEARCH: {
    HTTP: config.get<string>('elastic-search.http'),
    AUTH: {
      USERNAME: config.get<string>('elastic-search.auth.username'),
      PASSWORD: config.get<string>('elastic-search.auth.password')
    },
    SSL: {
      CA: config.get<string>('elastic-search.ssl.ca')
    },
    HOSTNAME: config.get<string>('elastic-search.hostname'),
    PORT: config.get<number>('elastic-search.port'),
    INDEXES: {
      VIDEOS: config.get<string>('elastic-search.indexes.videos'),
      CHANNELS: config.get<string>('elastic-search.indexes.channels'),
      PLAYLISTS: config.get<string>('elastic-search.indexes.playlists')
    }
  },
  LOG: {
    LEVEL: config.get<string>('log.level')
  },
  SEARCH_INSTANCE: {
    NAME: config.get<string>('search-instance.name'),
    NAME_IMAGE: config.get<string>('search-instance.name_image'),
    SEARCH_IMAGE: config.get<string>('search-instance.search_image'),
    DESCRIPTION: config.get<string>('search-instance.description'),
    LEGAL_NOTICES_URL: config.get<string>('search-instance.legal_notices_url'),
    THEME: config.get<string>('search-instance.theme')
  },
  VIDEOS_SEARCH: {
    BOOST_LANGUAGES: {
      ENABLED: config.get<boolean>('videos-search.boost-languages.enabled')
    },
    SEARCH_FIELDS: {
      NAME: {
        FIELD_NAME: 'name',
        BOOST: config.get<number>('videos-search.search-fields.name.boost'),
        MATCH_TYPE: config.get<string>('videos-search.search-fields.name.match_type')
      },
      DESCRIPTION: {
        FIELD_NAME: 'description',
        BOOST: config.get<number>('videos-search.search-fields.description.boost'),
        MATCH_TYPE: config.get<string>('videos-search.search-fields.description.match_type')
      },
      TAGS: {
        FIELD_NAME: 'tags',
        BOOST: config.get<number>('videos-search.search-fields.tags.boost'),
        MATCH_TYPE: config.get<string>('videos-search.search-fields.tags.match_type')
      },
      ACCOUNT_DISPLAY_NAME: {
        FIELD_NAME: 'account.displayName',
        BOOST: config.get<number>('videos-search.search-fields.account-display-name.boost'),
        MATCH_TYPE: config.get<string>('videos-search.search-fields.account-display-name.match_type')
      },
      CHANNEL_DISPLAY_NAME: {
        FIELD_NAME: 'channel.displayName',
        BOOST: config.get<number>('videos-search.search-fields.channel-display-name.boost'),
        MATCH_TYPE: config.get<string>('videos-search.search-fields.channel-display-name.match_type')
      }
    }
  },
  CHANNELS_SEARCH: {
    SEARCH_FIELDS: {
      NAME: {
        FIELD_NAME: 'name',
        BOOST: config.get<number>('channels-search.search-fields.name.boost'),
        MATCH_TYPE: config.get<string>('channels-search.search-fields.name.match_type')
      },
      DESCRIPTION: {
        FIELD_NAME: 'description',
        BOOST: config.get<number>('channels-search.search-fields.description.boost'),
        MATCH_TYPE: config.get<string>('channels-search.search-fields.description.match_type')
      },
      DISPLAY_NAME: {
        FIELD_NAME: 'displayName',
        BOOST: config.get<number>('channels-search.search-fields.display-name.boost'),
        MATCH_TYPE: config.get<string>('channels-search.search-fields.display-name.match_type')
      },
      ACCOUNT_DISPLAY_NAME: {
        FIELD_NAME: 'ownerAccount.displayName',
        BOOST: config.get<number>('channels-search.search-fields.account-display-name.boost'),
        MATCH_TYPE: config.get<string>('channels-search.search-fields.account-display-name.match_type')
      }
    }
  },
  PLAYLISTS_SEARCH: {
    SEARCH_FIELDS: {
      DISPLAY_NAME: {
        FIELD_NAME: 'displayName',
        BOOST: config.get<number>('playlists-search.search-fields.display-name.boost'),
        MATCH_TYPE: config.get<string>('playlists-search.search-fields.display-name.match_type')
      },
      DESCRIPTION: {
        FIELD_NAME: 'description',
        BOOST: config.get<number>('playlists-search.search-fields.description.boost'),
        MATCH_TYPE: config.get<string>('playlists-search.search-fields.description.match_type')
      }
    }
  },
  INSTANCES_INDEX: {
    URL: config.get<string>('instances-index.url'),
    PUBLIC_URL: config.get<string>('instances-index.public_url'),
    WHITELIST: {
      ENABLED: config.get<boolean>('instances-index.whitelist.enabled'),
      HOSTS: config.get<string[]>('instances-index.whitelist.hosts')
    }
  },
  API: {
    BLACKLIST: {
      ENABLED: config.get<boolean>('api.blacklist.enabled'),
      HOSTS: config.get<string[]>('api.blacklist.hosts')
    }
  }
}

const SORTABLE_COLUMNS = {
  VIDEOS_SEARCH: [ 'name', 'duration', 'createdAt', 'publishedAt', 'originallyPublishedAt', 'views', 'likes', 'match' ],
  CHANNELS_SEARCH: [ 'match', 'displayName', 'createdAt' ],
  PLAYLISTS_SEARCH: [ 'match', 'displayName', 'createdAt' ]
}

const PAGINATION_START = {
  MAX: 9000
}

const PAGINATION_COUNT = {
  DEFAULT: 20,
  MAX: 500
}

const SCHEDULER_INTERVALS_MS = {
  indexation: 60000 * 60 * 24 // 24 hours
}

const INDEXER_COUNT = 10
const INDEXER_LIMIT = 500000

const INDEXER_HOST_CONCURRENCY = 3
const INDEXER_QUEUE_CONCURRENCY = 3

const REQUESTS = {
  MAX_RETRIES: 10,
  WAIT: 10000 // 10 seconds
}

const ELASTIC_SEARCH_QUERY = {
  FUZZINESS: 'AUTO:4,7',
  OPERATOR: 'OR',
  MINIMUM_SHOULD_MATCH: '3<75%',
  BOOST_LANGUAGE_VALUE: 1,
  MALUS_LANGUAGE_VALUE: 0.5,
  VIDEOS_MULTI_MATCH_FIELDS: buildMatchFieldConfig(CONFIG.VIDEOS_SEARCH.SEARCH_FIELDS),
  CHANNELS_MULTI_MATCH_FIELDS: buildMatchFieldConfig(CONFIG.CHANNELS_SEARCH.SEARCH_FIELDS),
  PLAYLISTS_MULTI_MATCH_FIELDS: buildMatchFieldConfig(CONFIG.PLAYLISTS_SEARCH.SEARCH_FIELDS)
}

function getWebserverUrl () {
  if (CONFIG.WEBSERVER.PORT === 80 || CONFIG.WEBSERVER.PORT === 443) {
    return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME
  }

  return CONFIG.WEBSERVER.SCHEME + '://' + CONFIG.WEBSERVER.HOSTNAME + ':' + CONFIG.WEBSERVER.PORT
}

function buildMatchFieldConfig (fields: { [name: string]: { BOOST: number, FIELD_NAME: string, MATCH_TYPE: string } }) {
  const selectFields = (matchType: 'phrase' | 'default') => {
    return Object.keys(fields)
                 .filter(fieldName => fields[fieldName].MATCH_TYPE === matchType)
                 .map(fieldName => fields[fieldName])
  }

  const buildMultiMatch = (fields: { BOOST: number, FIELD_NAME: string }[]) => {
    return fields.map(fieldObj => {
      if (fieldObj.BOOST <= 0) return ''

      return `${fieldObj.FIELD_NAME}^${fieldObj.BOOST}`
    })
    .filter(v => !!v)
  }

  return {
    default: buildMultiMatch(selectFields('default')),
    phrase: buildMultiMatch(selectFields('phrase'))
  }
}

if (isTestInstance()) {
  SCHEDULER_INTERVALS_MS.indexation = 1000 * 60 * 5 // 5 minutes
}

export {
  getWebserverUrl,

  CONFIG,
  API_VERSION,
  PAGINATION_COUNT,
  PAGINATION_START,
  SORTABLE_COLUMNS,
  INDEXER_QUEUE_CONCURRENCY,
  SCHEDULER_INTERVALS_MS,
  INDEXER_HOST_CONCURRENCY,
  INDEXER_COUNT,
  INDEXER_LIMIT,
  REQUESTS,
  ELASTIC_SEARCH_QUERY
}
