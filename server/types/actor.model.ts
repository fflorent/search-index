import { ActorImage } from '../../PeerTube/shared/models'

export type AdditionalActorAttributes = {
  handle: string
  url: string

  avatar: ActorImageExtended
  avatars: ActorImageExtended[]
}

export type ActorImageExtended = ActorImage & { url: string }
