import { VideoPlaylistsSearchQuery as PeerTubePlaylistsSearchQuery } from '../../../PeerTube/shared/models'
import { CommonSearch } from './common-search.model'

export type PlaylistsSearchQuery = PeerTubePlaylistsSearchQuery & CommonSearch
