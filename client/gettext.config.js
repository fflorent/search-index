module.exports = {
  output: {
    path: './src/locale',
    jsonPath: '../translations',
    splitJson: true,
    linguas: false,
    locales: [
      'en_US',
      'fr_FR',
      'de',
      'es',
      'gl',
      'ru',
      'oc',
      'ja',
      'sv',
      'hr',
      'nl',
      'pl',
      'sq',
      'gd',
      'el',
      'bn',
      'it',
      'pt_BR',
      'uk',
      'cs'
    ]
  }
}
